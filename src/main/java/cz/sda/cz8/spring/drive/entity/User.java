package cz.sda.cz8.spring.drive.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Uzivatel")
public class User {
    @Id
    private String username;
    @Column
    private String password;
    @Column
    private String roles;
}
