package cz.sda.cz8.spring.drive.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.sda.cz8.spring.drive.FileUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Data
@ToString(exclude = "dir")
@Slf4j
public class SpringFile {
    private String fileName;
    private long size;
    private String hash;
    @JsonIgnore
    private SpringDir dir;

    public SpringFile(String fileName, SpringDir dir) {
        this.fileName = fileName;
        this.dir = dir;
        try {
            computeProperties();
        }catch (Exception e){
            log.error("Cannot compute size",e);
        }
    }

    private void computeProperties() throws Exception {
        size =  Files.size(Path.of(dir.getPath(),fileName));
        hash = FileUtil.computeMd5(Path.of(dir.getPath(),fileName).toFile().getPath());
    }
}
