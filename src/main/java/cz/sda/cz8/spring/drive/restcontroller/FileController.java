package cz.sda.cz8.spring.drive.restcontroller;

import cz.sda.cz8.spring.drive.entity.SpringFile;
import cz.sda.cz8.spring.drive.service.DriveManager;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("file")
@Slf4j
public class FileController {

    DriveManager manager;

    public FileController(DriveManager manager) {
        this.manager = manager;
    }

    @GetMapping
    public ResponseEntity<Resource> getFile(@RequestParam String fileName, @Autowired HttpServletRequest request) {
        Resource resource = manager.getFileContent(fileName);// fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @RequestMapping(method = RequestMethod.HEAD)
    public ResponseEntity getFileInfo(@RequestParam String fileName) {
        Optional<SpringFile> fileInfo = manager.getFileInfo(fileName);
        if (fileInfo.isPresent()) {
           return ResponseEntity.ok()
                   .header("fileName",fileInfo.get().getFileName())
                   .header("size",String.valueOf(fileInfo.get().getSize()))
                   .header("hash",fileInfo.get().getHash()).build();
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public void saveFile(@RequestParam String fileName, @RequestBody MultipartFile file) throws IOException {

        manager.saveFile(fileName,file);

    }
}
