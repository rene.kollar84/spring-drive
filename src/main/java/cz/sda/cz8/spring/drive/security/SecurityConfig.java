package cz.sda.cz8.spring.drive.security;

import cz.sda.cz8.spring.drive.dao.LoginRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)

public class SecurityConfig {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests()
                .requestMatchers("/**").hasAnyRole("USER", "ADMIN")      //vsechno musi byt pod userem a heslem a ten musi mit pridelenu roli, jinak to nemaka
                .and()
                .httpBasic()                                                            //basic autentifikace - soucasti http requestu bude header Authentication Basic  Base64(username:password)
                .and()
                .formLogin()
                .and()
                .logout()
                .and()
                .headers().frameOptions().disable()
                .and()
                .csrf().disable();
        return http.build();
    }
    @Bean                                                                               //definuje jak prevzit info o loginu
    public AuthenticationManager authenticationManager(HttpSecurity http, UserDetailsService userDetail)
            throws Exception {


        AuthenticationManagerBuilder auth = http.getSharedObject(AuthenticationManagerBuilder.class);

        auth.userDetailsService(userDetail);
        return auth.build();

    }
    @Bean                                                                               //uloziste uzivatelu
    public UserDetailsManager userDetailsService(LoginRepository repository) {

        User.UserBuilder users = User.withDefaultPasswordEncoder();

        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
      //  manager.createUser(users.username("uzv").password("uzv").roles("USER").build());
        manager.createUser(users.username("adm").password("adm").roles("ADMIN").build());
        repository.findAll().forEach(userFromDB ->  manager.createUser(users.username(userFromDB.getUsername())
                .password(userFromDB.getPassword()).roles(userFromDB.getRoles().split(",")).build()));
        return manager;

    }

}
