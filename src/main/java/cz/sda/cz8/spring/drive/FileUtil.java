package cz.sda.cz8.spring.drive;

import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;

public class FileUtil {
    public static String computeMd5(String path) throws Exception{
        byte[] data = Files.readAllBytes(Paths.get(path));
        byte[] hash = MessageDigest.getInstance("MD5").digest(data);
        String checksum = new BigInteger(1, hash).toString(16);
        return checksum;
    }
}
