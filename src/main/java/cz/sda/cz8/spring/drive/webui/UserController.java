package cz.sda.cz8.spring.drive.webui;

import cz.sda.cz8.spring.drive.entity.User;
import cz.sda.cz8.spring.drive.service.MyUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    MyUserService userService;

    @GetMapping
    public String showForm(final ModelMap modelMap) {
        modelMap.put("usr",new User());
        return "create-user";
    }

    @GetMapping("edit")
    @Secured("ROLE_ADMIN")
    public String editUser(final ModelMap modelMap) {
        modelMap.put("usr",new User());
        return "edit-user";
    }

    @PostMapping("editUser")
    @Secured("ROLE_ADMIN")
    public String editUser(@ModelAttribute("usr")  User user){

       User userFromDb =  userService.findByLogin(user.getUsername());
       userFromDb.setRoles(user.getRoles());
       userService.editUser(userFromDb);
        return "created";
    }

    @PostMapping("createUser")
    public String createUser(@ModelAttribute("usr")  User user){
        if("".equals(user.getRoles()) || user.getRoles() == null){
            user.setRoles("USER");
        }
        userService.saveUser(user);
        return "created";
    }


}
