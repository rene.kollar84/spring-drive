package cz.sda.cz8.spring.drive;

import cz.sda.cz8.spring.drive.entity.SpringDir;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.nio.file.Paths;

@SpringBootApplication
public class SpringDriveApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDriveApplication.class, args);
    }

    @Bean
    public File getFile(@Value("${drive.dir:spring-drive}") String path) {
        File file = new File(path);
       // Paths.get().
        return file;
    }

    @Bean
    public SpringDir springDir(File root) {
        if (!root.exists()) {
            root.mkdirs();
        }
        return new SpringDir(root.getAbsolutePath(), null);
    }
}


