package cz.sda.cz8.spring.drive.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.File;
import java.util.ArrayList;

@Getter
@Setter
@ToString(exclude = "parent")
public class SpringDir {
    @JsonIgnore
    private String name;
    private String path;
    @JsonBackReference
    private SpringDir parent;

    private ArrayList<SpringFile> files = new ArrayList<>();

    @JsonManagedReference
    private ArrayList<SpringDir> dirs = new ArrayList<>();

    public SpringDir(String path, SpringDir parent) {
        this.path = path;
        this.parent = parent;
        String[] split = path.split("/");
        name = split[split.length - 1];
        initChilds(this);
    }

    private void initChilds(SpringDir dir) {
        File file = new File(dir.path);
        for (File f : file.listFiles()) {
            if (f.isFile()) {
                dir.files.add(new SpringFile(f.getName(), dir));
            }
            if (f.isDirectory()) {
                dir.dirs.add(new SpringDir(f.getAbsolutePath(), dir));
            }
        }
    }
}
