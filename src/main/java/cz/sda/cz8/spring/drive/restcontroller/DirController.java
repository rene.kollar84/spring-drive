package cz.sda.cz8.spring.drive.restcontroller;

import cz.sda.cz8.spring.drive.entity.SpringDir;
import cz.sda.cz8.spring.drive.service.DriveManager;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Validated
@RequestMapping("dir")
public class DirController {
    @Autowired
    DriveManager manager;

    @GetMapping
    public ResponseEntity<SpringDir> getDir() {
        return new ResponseEntity<>(manager.getRoot(), HttpStatus.OK);
    }

    @PostMapping("{name}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<SpringDir> createDir(@Valid
                                               @Pattern(regexp = "[a-zA-Z0-9]{1,255}")
                                               @PathVariable String name,
                                               @RequestBody String path) {
        SpringDir root;
        if (path == null) {
            root = manager.getRoot();
        } else {
            root = manager.findByPath(path);
        }
        SpringDir dir = manager.createDir(root, name);
        return new ResponseEntity<>(dir, HttpStatus.OK);
    }

    @DeleteMapping
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Boolean> deleteDir(@RequestBody String path) {
        boolean ret = manager.delete(path);
        return new ResponseEntity<>(ret, ret ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }
    @PatchMapping("{newName}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Boolean> renameDir(@RequestBody String path,@PathVariable String newName) {
        boolean ret = manager.rename(path, newName);
        return new ResponseEntity<>(ret, ret ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }
}
