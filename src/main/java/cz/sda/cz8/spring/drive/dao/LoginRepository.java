package cz.sda.cz8.spring.drive.dao;

import cz.sda.cz8.spring.drive.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface LoginRepository extends CrudRepository<User, String> {


}
