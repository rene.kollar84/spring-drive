package cz.sda.cz8.spring.drive;

import cz.sda.cz8.spring.drive.service.DriveManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class StartupInfo implements CommandLineRunner {

    DriveManager manager;

    public StartupInfo(DriveManager manager) {
        this.manager = manager;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info(manager.getRoot().toString());
    }
}
