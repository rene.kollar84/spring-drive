package cz.sda.cz8.spring.drive.service;

import cz.sda.cz8.spring.drive.dao.LoginRepository;
import cz.sda.cz8.spring.drive.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MyUserService {

    @Autowired
    UserDetailsManager userManager;

    @Autowired
    LoginRepository loginRepository;

    public void saveUser(User user) {
        //user.getRoles() vraci "ADMIN", "USER" i.e

        org.springframework.security.core.userdetails.User.UserBuilder users =
                org.springframework.security.core.userdetails.User.withDefaultPasswordEncoder();

        userManager.createUser(users.username(user.getUsername())
                .password(user.getPassword()).roles(user.getRoles().split(",")).build());
        loginRepository.save(user);

    }

    public void editUser(User user) {
        //user.getRoles() vraci "ADMIN", "USER" i.e

        org.springframework.security.core.userdetails.User.UserBuilder users =
                org.springframework.security.core.userdetails.User.withDefaultPasswordEncoder();
        userManager.deleteUser(user.getUsername());

        userManager.createUser(users.username(user.getUsername())
                .password(user.getPassword()).roles(user.getRoles().split(",")).build());
        loginRepository.save(user);

    }


    public User findByLogin(String username) {
        Optional<User> byId = loginRepository.findById(username);
        return byId.get();
    }
}
