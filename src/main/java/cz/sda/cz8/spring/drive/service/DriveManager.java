package cz.sda.cz8.spring.drive.service;

import cz.sda.cz8.spring.drive.entity.SpringDir;
import cz.sda.cz8.spring.drive.entity.SpringFile;
import org.springframework.core.io.AbstractFileResolvingResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URL;
import java.nio.file.*;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

@Component
public class DriveManager {

    SpringDir root;

    public DriveManager(SpringDir root) {
        this.root = root;
    }

    public SpringDir getRoot() {
        return root;
    }

    public SpringDir createDir(SpringDir parent, String newDirname) {
        Path path = Paths.get(parent.getPath(), newDirname);

        if (!path.toFile().exists()) {
            path.toFile().mkdirs();
            SpringDir springDir = new SpringDir(path.toFile().getAbsolutePath(), parent);
            root.getDirs().add(springDir);
            return springDir;
        }
        throw new RuntimeException("Already exists " + newDirname);

    }

    public SpringDir findByPath(String path) {
        String[] dirNames = path.split("/");
        SpringDir actual = root;
        for (String dirName : dirNames) {
            Optional<SpringDir> first = actual.getDirs().stream().filter(d -> dirName.equals(d.getName())).findFirst();
            if (first.isPresent()) {
                actual = first.get();
            } else {
                throw new RuntimeException("");//TODO create special exception and create exception handler
            }
        }
        return actual;
    }

    public Optional<SpringFile> getFileInfo(String fullName) {
        //fullName = a/b/ahoj.txt
        String[] pAn = extractPathAndFilename(fullName);
        SpringDir byPath = findByPath(pAn[0]);
        Optional<SpringFile> first = byPath.getFiles().stream().filter(sf -> sf.getFileName().equals(pAn[1])).findFirst();
        return first;

    }

    private String[] extractPathAndFilename(String fullName) {
        String[] paths = fullName.split("/");
        String ret = "";
        for (int i = 0; i < paths.length - 1; i++) {
            ret += paths[i] + "/";
        }
        return new String[]{ret, paths[paths.length - 1]};
    }

    public Resource getFileContent(String name) {

        return new AbstractFileResolvingResource() {
            @Override
            public String getFilename() {
                String[] split = name.split("/");

                return split[split.length - 1];
            }

            @Override
            public URL getURL() throws IOException {
                return Paths.get(getRoot().getPath(), name).toFile().toURL();
            }

            @Override
            public String getDescription() {
                return name;
            }

            @Override
            public InputStream getInputStream() throws IOException {
                try (FileInputStream fi = new FileInputStream(Paths.get(getRoot().getPath(), name).toFile())) {
                    byte[] bytes = new BufferedInputStream(fi).readAllBytes();
                    return new ByteArrayInputStream(bytes);
                }
            }
        };
    }

    public void saveFile(String fileName, MultipartFile file) throws IOException {
        String[] pAn = extractPathAndFilename(fileName);
        SpringDir byPath = findByPath(pAn[0]);

        Files.write(Paths.get(byPath.getPath(),pAn[1]),file.getBytes(), StandardOpenOption.CREATE_NEW);

        SpringFile springFile = new SpringFile(pAn[1], byPath);
        byPath.getFiles().add(springFile);
    }

    public boolean delete(String path) {
        try {

            SpringDir byPath = findByPath(path);
            boolean delete = new File(byPath.getPath()).delete();
            byPath.getParent().getDirs().removeIf(springDir -> springDir == byPath);
            return delete;
        }
        catch(RuntimeException e){
            return false;
        }

    }

    public boolean rename(String path, String newName) {
        try {

            SpringDir byPath = findByPath(path);
            // spring-drive/DiR2  spring-drive/dir3
            String[] split = byPath.getPath().split("/");
            split[split.length-1] = newName;
            File f = new File(Arrays.stream(split).reduce((a,b)->a+File.separator+b).get());
            boolean rename = new File(byPath.getPath()).renameTo(f);
            byPath.setPath(f.getAbsolutePath());
            byPath.setName(newName);
            return rename;
        }
        catch(RuntimeException e){
            return false;
        }

    }
}
